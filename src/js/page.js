$(document).ready(function() {

	//show-more rest-menu
	$(".show-more").click(function() {
		var txt = $(".show-more-content").is(':visible') ? 'show more' : 'show less';
		$(".show-more span").text(txt);
		$(".show-more-content").slideToggle();
		$(this).children("i").toggleClass("fa-angle-down fa-angle-up");
	});

	//owl.carousel starting
	$('.owl-carousel').owlCarousel({
		loop: true,
		margin: 10,
		responsiveClass: true,
		dots: true,
		dotsEach: true,
		responsive: {
			0: {
				items: 1
			},
			600: {
				items: 3
			},
			1000: {
				items: 4,
				loop: false,
				margin: 50
			}
		}
	})

	// sticky header
	var AnimatedHeader = (function() {

		var docElem = document.documentElement,
			header = document.querySelector( '.menu-wrapper' ),
			didScroll = false,
			changeHeaderOn = 100;

		function init() {
			window.addEventListener( 'scroll', function( event ) {
				if( !didScroll ) {
					didScroll = true;
					setTimeout( scrollPage, 250 );
				}
			}, false );
		}

		function scrollPage() {
			var sy = scrollY();
			if ($(window).width() > 768) {
				if (sy >= changeHeaderOn) {
					$('.menu-wrapper').addClass('menu-wrapper-shrink');
				}
				else {
					$('.menu-wrapper').removeClass('menu-wrapper-shrink');
				}
			}
			didScroll = false;
		}

		function scrollY() {
			return window.pageYOffset || docElem.scrollTop;
		}

		init();

	})();

});

//hamburger icon
document.querySelector( "#nav-toggle" )
	.addEventListener( "click", function() {
		this.classList.toggle( "active" );
});
