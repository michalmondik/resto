module.exports = function (grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		copy: {
			fonts: {
				files: [
					{
						expand: true,
						src: [
							'bower_components/bootstrap/fonts/*',
							'bower_components/fontawesome/fonts/*',
							'src/src-fonts/*'
						],
						dest: 'fonts/',
						flatten: true
					}
				]
			},
			jQueryMap: {
				files: [
					{
						expand: true,
						src: [
							'bower_components/jquery/dist/jquery.min.map'
						],
						dest: 'js/',
						flatten: true
					}
				]
			}
		},

		less: {
			default: {
				files: {
					'css/styles.css': 'src/less/styles.less'
				},
				options: {
					compress: true
				}
			}
		},

		concat: {
			scripts: {
				src: ['<%= pkg.jsFiles %>'],
				dest: 'js/scripts.js'
			}
		},

		uglify: {
			options: {
				banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
			},
			scripts: {
				files: {
					'js/scripts.min.js': ['<%= concat.scripts.dest %>']
				}
			}
		},

		watch: {
			less: {
				files: ['src/less/**/*.less'],
				tasks: ['less'],
				options: {
					interrupt: true
				}
			},
			js: {
				files: ['src/js/**/*.js', 'package.json'],
				tasks: ['js'],
				options: {
					interrupt: true
				}
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.registerTask('js', ['concat', 'uglify']);
	grunt.registerTask('default', ['copy', 'less', 'js']);
};
